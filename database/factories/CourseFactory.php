<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Course;
use Faker\Generator as Faker;

$factory->define(Course::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'content' => $faker->content,
        'day' => $faker->day,
        'time' => $faker->time,
        'remember_token' => Str::random(10),
    ];
});
