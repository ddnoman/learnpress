<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
class CoursesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('courses')->insert([
            'name' => Str::random(10),
            'content' => Str::random(10),
            'day' => Str::random(10),
            'time' => Str::random(10),
        ]);
    }
    public function handle()
    {
        Tag::truncate();
        factory(Course::class, 5)->create();
    }
}
