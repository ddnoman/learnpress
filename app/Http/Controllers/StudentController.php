<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StudentValidationRequest;

use App\Course;
use App\Student;
use App\Teacher;


class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $students = Student::all();
        $course = Course::all();
        return view('admin.students.index', compact('students', 'course'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $courses = Course::all();
        $teachers = Teacher::all();
        return view('admin.students.create', compact('courses', 'teachers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StudentValidationRequest $request)
    {
        try {
            $filename = sprintf('thumbnail_%s.jpg', random_int(1, 1000));
            if ($request->hasFile('thumbnail'))
                $filename = $request->file('thumbnail')->storeAs('teachers', $filename, 'public');

            $student = [
                'name' => $request->name,
                'email' => $request->email,
                'contact' => $request->contact,
                'description' => $request->description,
                'teacher_id' => $request->teacher,
                'experience' => $request->experience,
                'dob' => $request->dob,
                'address' => $request->address,
                'thumbnail' => $filename,
            ];

            $student = Student::create($student);
            $student->courses()->attach($request->input('course_id'));

            if ($student) {
                return redirect()->route('students.index')->with('success', 'Student Created successfully');

            }
        }catch (\Exception $exception){

            return redirect()
                ->route('students.index')
                ->with('error', 'There is an issue with your submission, please try again later.');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $student = Student::find($id);
        return view('admin.students.show', compact('student'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function edit(Student $student)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Student $student)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Student::find($id)->delete();

        return redirect()->route('students.index')->with('deleted','Students Deleted Successfully');
    }
}
