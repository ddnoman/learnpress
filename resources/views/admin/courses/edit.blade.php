@extends('admin.layouts.app')
@section('title', 'Courses')

@push('stylesheets')

@endpush
@section('content')

    <div class="col" style="margin-top: 50px">
      <div class="row">
          <div class="col-md-6">
              <h4 style="margin-bottom: 30px;font-weight: bold">Edit  Course</h4>
          </div>
          <div class="col-md-6" style="text-align: right">
              <a href="{{route('courses.index')}}" class="btn btn-info">Back to courses</a>
          </div>
      </div>

        <form action="{{ route('courses.update', $course->id) }}" method="post" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="name">Course Name</label>
                <input type="text" class="form-control" name="name" id="name" value="{{$course->name}}">
                <span style="color: #ff0000; font-size: 13px; font-weight: bold">@error('name')
                    {{ $message }}
                    @enderror</span>
            </div>

            <label for="comment">Description:</label>
            <textarea class="form-control" rows="3" id="description" name="description">{{$course->description}}</textarea>
            <span style="color: #ff0000; font-size: 13px; font-weight: bold">@error('description')
                {{ $message }}
                @enderror</span>

            <div class="form-group">
                <label for="day">Select Days</label>
                <select  multiple="" class="form-control" id="day[]" name="day[]">
                    <option value="mon" @if(in_array('mon',$selectDays)) selected @endif>Monday</option>
                    <option value="tue" @if(in_array('tue',$selectDays)) selected @endif>Tuesday</option>
                    <option value="wed" @if(in_array('wed',$selectDays)) selected @endif>Wednesday</option>
                    <option value="thur" @if(in_array('thur',$selectDays)) selected @endif>Thursday</option>
                    <option value="fri" @if(in_array('fri',$selectDays)) selected @endif>Friday</option>
                </select>
                <span style="color: #ff0000; font-size: 13px; font-weight: bold">@error('day')
                    {{ $message }}
                    @enderror</span>
            </div>

            <div class="form-group col-xs-10 col-md-4 col-lg-6 ">
                <label for="name">Price (Pkr)</label>
                <input type="text" class="form-control" id="price" name="price"  value="{{$course->price}}">
                <span style="color: #ff0000; font-size: 13px; font-weight: bold">@error('price')
                    {{ $message }}
                    @enderror</span>
            </div>

            <div class="form-group col-xs-10 col-md-4 col-lg-6 ">
                <label for="name">Discount (%)</label>
                <input type="text" class="form-control" id="discount" name="discount"  value="{{$course->discount}}">
                <span style="color: #ff0000; font-size: 13px; font-weight: bold">@error('discount')
                    {{ $message }}
                    @enderror</span>
            </div>

            <div class="form-group col-xs-10 col-md-4 col-lg-6 ">
                <label for="name">Time From</label>
                <input type="datetime-local" class="form-control"
                       id="timefrom" name="start_time" value="{{ Carbon\Carbon::parse($course->start_time)->format('Y-m-d\TH:i') }}">
                <span style="color: #ff0000; font-size: 13px; font-weight: bold">@error('start_time')
                    {{ $message }}
                    @enderror</span>
            </div>

            <div class="form-group col-xs-10 col-md-4 col-lg-6">
                <label for="name">Time To</label>
                <input type="datetime-local" class="form-control"
                       id="timeto" name="end_time" value="{{ Carbon\Carbon::parse($course->end_time)->format('Y-m-d\TH:i') }}">
                <span style="color: #ff0000; font-size: 13px; font-weight: bold">@error('end_time')
                    {{ $message }}
                    @enderror</span>
            </div>

            <div class="form-group">
                <label for="thumbnail">Upload Image:</label>
                <img width="7%" class="" src="{{ URL::asset('storage/'.$course->thumbnail) }}">
                <input type="file" class="form-control" id="thumbnail" name="thumbnail">
                <span style="color: #ff0000; font-size: 13px; font-weight: bold">@error('thumbnail')
                    {{ $message }}
                    @enderror</span>
            </div>

            <button type="submit" class="btn btn-info" style="width: 100%">Update Course</button>
        </form>
    </div>

@endsection


@push('scripts')

@endpush
