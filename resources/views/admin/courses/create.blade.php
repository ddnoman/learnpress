@extends('admin.layouts.app')
@section('title', 'Courses')

@push('stylesheets')

@endpush
@section('content')

<div class="col" style="margin-top: 20px">

        <div class="row">
            <div class="col-12 col-md-4">
                <h3 class="text-bold-500" style="font-weight: bold">Add New Course</h3>
            </div>
            <div class="col-12 col-md-8 text-right">
                <a href="{{ route('courses.index') }}" class="btn btn-info">All courses</a>
                <a href="{{ route('courses.create') }}" class="btn btn-info">Add New Course</a>
            </div>
        </div>
    <form action="{{ route('courses.store') }}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label for="name">Course Name</label>
            <input type="text" class="form-control" name="name" id="name">
            <span style="color: #ff0000; font-size: 13px; font-weight: bold">@error('name')
                {{ $message }}
                @enderror</span>
        </div>

        <label for="comment">Description:</label>
        <textarea class="form-control" rows="2" id="description" name="description"></textarea>
        <span style="color: #ff0000; font-size: 13px; font-weight: bold">@error('description')
            {{ $message }}
            @enderror</span>
        <div class="form-group">
            <label for="day">Select Days</label>
            <select  multiple="" class="form-control" id="day[]" name="day[]">
                <option value="mon">Monday</option>
                <option value="tue">Tuesday</option>
                <option value="wed">Wednesday</option>
                <option value="thur">Thursday</option>
                <option value="fri">Friday</option>
            </select>
            <span style="color: #ff0000; font-size: 13px; font-weight: bold">@error('day')
                {{ $message }}
                @enderror</span>

        </div>


        <div class="form-group col-xs-10 col-md-4 col-lg-6 ">
            <label for="name">Price (Pkr) </label>
            <input type="text" class="form-control" id="price" name="price">
            <span style="color: #ff0000; font-size: 13px; font-weight: bold">@error('price')
                {{ $message }}
                @enderror</span>
        </div>

        <div class="form-group col-xs-10 col-md-4 col-lg-6 ">
            <label for="name">Discount (%)</label>
            <input type="text" class="form-control" id="discount" name="discount" value="0">
        </div>


        <div class="form-group col-xs-10 col-md-4 col-lg-6 ">
            <label for="name">Start Date $ Time</label>
            <input type="datetime-local" class="form-control" id="timefrom" name="start_time">
            <span style="color: #ff0000; font-size: 13px; font-weight: bold">@error('start_time')
                {{ $message }}
                @enderror</span>
        </div>

        <div class="form-group col-xs-10 col-md-4 col-lg-6">
            <label for="name">End Date & Time</label>
            <input type="datetime-local" class="form-control" id="timeto" name="end_time">
            <span style="color: #ff0000; font-size: 13px; font-weight: bold">@error('end_time')
                {{ $message }}
                @enderror</span>
        </div>

        <div class="form-group">
            <label for="thumbnail">Upload Image:</label>
            <input type="file" class="form-control" id="thumbnail" name="thumbnail">
            <span style="color: #ff0000; font-size: 13px; font-weight: bold">@error('thumbnail')
                {{ $message }}
                @enderror</span>
        </div>

        <button type="submit" class="btn btn-info" style="width: 100%">Add New Course</button>
    </form>
</div>

@endsection


@push('scripts')

@endpush
