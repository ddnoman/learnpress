@extends('admin.layouts.app')
@section('title', 'teacher')

@push('stylesheets')

@endpush
@section('content')

<div class="col" style="margin-top: 50px">
    <div class="row">
        <div class="col-12 col-md-4">
            <h3 class="text-bold-500" style="font-weight: bold">Add New Teacher</h3>
        </div>
        <div class="col-12 col-md-8 text-right">
            <a href="{{ route('teachers.index') }}" class="btn btn-info">All Teachers</a>
        </div>
    </div>
    <form action="{{ route('teachers.store') }}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="form-group col-xs-10 col-md-4 col-lg-4">
            <label for="name">Teacher Name</label>
            <input type="text" class="form-control" name="name" id="name">
            <span style="color: #ff0000; font-size: 13px; font-weight: bold">@error('name')
                {{ $message }}
                @enderror</span>
        </div>
        <div class="form-group col-xs-10  col-md-4 col-lg-4">
            <label for="name">Email</label>
            <input type="email" class="form-control" name="email" id="email">
            <span style="color: #ff0000; font-size: 13px; font-weight: bold">@error('email')
                {{ $message }}
                @enderror</span>
        </div>

        <div class="form-group col-xs-10 col-md-4 col-lg-4">
            <label for="name">Contact</label>
            <input type="text" class="form-control" name="contact" id="contact">
            <span style="color: #ff0000; font-size: 13px; font-weight: bold">@error('contact')
                {{ $message }}
                @enderror</span>
        </div>

        <div class="form-group col-xs-10 col-md-12 col-lg-12">
        <label for="comment">Description:</label>
        <textarea class="form-control" rows="3" id="description" name="description"></textarea>
            <span style="color: #ff0000; font-size: 13px; font-weight: bold">@error('description')
                {{ $message }}
                @enderror</span>
        </div>

        <div class="form-group col-xs-10 col-md-12 col-lg-12">
            <label for="day">Select Course</label>
            <select  multiple="" class="form-control" id="course[]" name="course[]">
              @if(!$courses->isEmpty())
                    @foreach($courses as $course)
                        <option value="{{$course->id}}">{{$course->name}}</option>
                    @endforeach
{{--                @else--}}
{{--                    <option value="No Course">No Course Available </option>--}}
                @endif
            </select>
            <span style="color: #ff0000; font-size: 13px; font-weight: bold">@error('course')
                {{ $message }}
                @enderror</span>
        </div>

        <div class="form-group col-xs-10 col-md-4 col-lg-4">
            <label for="name">Experience (Years) </label>
            <input type="text" class="form-control" name="experience" id="experience">
            <span style="color: #ff0000; font-size: 13px; font-weight: bold">@error('experience')
                {{ $message }}
                @enderror</span>
        </div>

        <div class="form-group col-xs-10 col-md-4 col-lg-4">
            <label for="name">Address</label>
            <input type="text" class="form-control" name="address" id="address">
            <span style="color: #ff0000; font-size: 13px; font-weight: bold">@error('address')
                {{ $message }}
                @enderror</span>
        </div>

        <div class="form-group col-xs-10 col-md-4 col-lg-4">
            <label for="name">Image</label>
            <input type="file" class="form-control" name="thumbnail" id="thumbnail">
            <span style="color: #ff0000; font-size: 13px; font-weight: bold">@error('thumbnail')
                {{ $message }}
                @enderror</span>
        </div>


        <button type="submit" class="btn btn-info " style="width: 100%">Add new Teacher</button>
    </form>
</div>

@endsection


@push('scripts')

@endpush
