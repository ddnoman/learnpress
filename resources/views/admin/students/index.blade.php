@extends('admin.layouts.app')
@section('title', 'Students')

@push('stylesheets')

@endpush
@section('content')
    <!-- Hoverable rows start -->
    <section class="section">
        <div class="row" id="table-hover-row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                       <div class="row">
                           <div class="col-12 col-md-4">
                               <h3 class="text-bold-500" style="font-weight: bold">All Students</h3>
                           </div>
                           <div class="col-12 col-md-8 text-right">
                               <a href="{{ route('students.index') }}" class="btn btn-info">All Students</a>
                               <a href="{{ route('students.create') }}" class="btn btn-info">Add New Students</a>
                           </div>
                       </div>
                    </div>
                    @if ($message = Session::get('error'))
                        <div class="alert alert-danger">
                            <p>{{ $message }}</p>
                        </div>
                    @endif

                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif
                    @if ($message = Session::get('deleted'))
                        <div class="alert alert-danger">
                            <p>{{ $message }}</p>
                        </div>
                    @endif

                    <div class="card-content">
                        <div class="card-body">

                        </div>
                        <!-- table hover -->
                        @if(!$students->isEmpty())
                        <div class="table-responsive">
                            <table class="table table-hover mb-0">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
{{--                                    <th>Course</th>--}}
                                    <th>Email</th>
                                    <th>Contact</th>
                                    <th>Experience</th>
                                    <th>Image</th>
                                    <th>Dob</th>
                                    <th>ACTION</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($students as $student)
                                <tr>
                                    <td class="text-bold-500">{{ $student->id }}</td>
                                    <td class="text-bold-500">{{ $student->name }}</td>
{{--                                    <td class="text-bold-500">--}}
{{--                                            {{$teacher->CourseTeacher->id}}--}}

{{--                                    </td>--}}
                                    <td class="text-bold-500">{{ $student->email }}</td>
                                    <td class="text-bold-500">{{ $student->contact }}</td>
                                    <td class="text-bold-500">{{ $student->experience }}</td>
                                    <td class="text-bold-500"><img width="50" style="border-radius: 50%" height="50"  class="" src="{{ URL::asset('storage/'.$student->thumbnail) }}"></td>

                                    <td class="text-bold-500">{{ $student->dob }}</td>

                                      <td>
                                          <form action="{{ route('students.destroy',$student->id) }}" method="POST">
                                              <a class="btn btn-info" href="{{ route('students.show',$student->id) }}"><i class="fas fa-eye"></i></a>
                                              <a class="btn btn-success" href="{{ route('students.edit',$student->id) }}"><i class="fas fa-edit"></i></a>

                                              @csrf
                                              @method('DELETE')

                                              <button href="{{route('students.destroy', $student->id)}}"  type="submit" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                          </form>
                                      </td>
                                </tr>
                                @endforeach


                                </tbody>
                            </table>
                        </div>
                            @else
                            <div class=" p-3">
                                <p class="alert text-center alert-info font-weight-bold">No Students Available</p>
                            </div>

                            @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Hoverable rows end -->


@endsection


@push('scripts')

@endpush
