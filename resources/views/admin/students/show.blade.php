@extends('admin.layouts.app')
@section('title', 'show')

@push('stylesheets')



@endpush
<style>
    body {
        padding: 0;
        margin: 0;
        font-family: 'Lato', sans-serif;
        color: #000;
    }

    .student-profile .card {
        border-radius: 10px;
    }

    .student-profile .card .card-header .profile_img {
        width: 150px;
        height: 150px;
        object-fit: cover;
        margin: 10px auto;
        border: 10px solid #ccc;
        border-radius: 50%;
    }

    .student-profile .card h3 {
        font-size: 20px;
        font-weight: 700;
    }

    .student-profile .card p {
        font-size: 16px;
        color: #000;
    }

    .student-profile .table th,
    .student-profile .table td {
        font-size: 14px;
        padding: 5px 10px;
        color: #000;
    }
</style>
@section('content')
    <!-- Student Profile -->



    <div class="row" style="margin-top: 50px">
        <div class="col-lg-4">
            <div class="card shadow-sm">
                <div class="card-header bg-transparent text-center">
                    <img class="profile_img img-" style="border-radius: 158px;width: 160px;height: 160px; border: 13px solid #4e4e4e61;"
                         src="{{ URL::asset('storage/'.$student->thumbnail) }}" alt="">
                    <h3>{{$student->name}}</h3>
                </div>
                <div class="card-body" style="margin-top: 30px;">
                    <a role="button" href="{{route('students.edit', $student->id)}}" class="text-warning btn btn-success" style="width: 100%; margin: 5px 0">Edit</a>
                    <form action="{{route('students.destroy', $student->id)}}" method="post">
                        @csrf
                        @method('DELETE')
                        <button style="width: 100%; " role="button" href="{{route('students.destroy', $student->id)}}" class="text-danger btn btn-danger font-weight-bold-700">Delete</button>
                    </form>
                    <a role="button" href="{{route('students.index')}}" class="text-warning btn btn-info" style="width: 100%;"> <i class="fas fa-arrow-left"></i>  Back To Students</a>

                </div>
            </div>
        </div>
        <div class="col-lg-8">
            <div class="card shadow-sm">
                <div class="card-header bg- border-0">
                    <h3 class="mb-0" style="font-weight: bold; margin-bottom: 20px; text-align: center" ></i>{{$student->name}} - Information</h3>
                </div>
                <div class="card-body pt-0">
                    <table class="table table-bordered">
                        <tr>
                            <th width="30%">Email</th>
                            <td width="2%">:</td>
                            <td>{{$student->email}}</td>
                        </tr>
                        <tr>
                            <th width="30%">Contact	</th>
                            <td width="2%">:</td>
                            <td>{{$student->contact}}</td>
                        </tr>
                        <tr>
                            <th width="30%">Experience</th>
                            <td width="2%">:</td>
                            <td>{{$student->experience}} Year</td>
                        </tr>
                        <tr>
                            <th width="30%">Address</th>
                            <td width="2%">:</td>
                            <td>{{$student->address}}</td>
                        </tr>

                    </table>

                    {{--                    <tr>--}}
                    {{--                        <th width="30%"><h4 style="font-weight: bold; margin-bottom: 20px;">Course</h4></th>--}}
                    {{--                        <td> @foreach($teacher->courses as $course)<span style="background-color: darkgreen; color: #fff;--}}
                    {{--                            padding: 6px 8px; font-size: 12px; font-weight: bold; margin: 5px 3px; border-radius: 20px;">{{ $course->name }}</span>@endforeach</td>--}}
                    {{--                    </tr>--}}


                    <div style="border: 1px solid rgba(0,0,0,0.24); padding: 5px; margin-top: 40px">
                        <th width="30%"><h4 style="font-weight: bold">Description</h4></th>
                        <td>{{$student->description}}</td>
                    </div>


                </div>
            </div>
        </div>
    </div>

@endsection


@push('scripts')

@endpush
